\set ON_ERROR_STOP
BEGIN;

-- Add an additional column, "leaf", to view "esum" to support "no-fork"
-- editing (editing an entry restricted to only leaf entries to prevent
-- the introduction of new edit forks.)

  -- Jmdictdb schema version id(s) to update database to and current
  -- schema version id(s) required for this update to be applied.
\set dbversion  '''2a6bd1'''
\set require    '''cda09c'''

\qecho Checking database version, 0 rows expected...
SELECT vchk (:require);                      -- Will raise error on failure.
INSERT INTO db(id) VALUES(x:dbversion::INT); -- Make this version active.
-- This update supercedes previous updates.
UPDATE db SET active=FALSE WHERE active AND  -- Deactivate all :require.
LPAD(TO_HEX(id),6,'0') IN (SELECT unnest(string_to_array(:require,',')));

-- Do the update.

-- This view is identical to the previous "esum" view but with the addition
-- of new column "leaf".
CREATE OR REPLACE VIEW esum AS (
    SELECT e.id,e.seq,e.stat,e.src,e.dfrm,e.unap,e.notes,e.srcnote,
        h.rtxt AS rdng,
        h.ktxt AS kanj,
        (SELECT ARRAY_TO_STRING(ARRAY_AGG( ss.gtxt ), ' / ')
         FROM
            (SELECT
                (SELECT ARRAY_TO_STRING(ARRAY_AGG(sg.txt), '; ')
                FROM (
                    SELECT g.txt
                    FROM gloss g
                    WHERE g.sens=s.sens AND g.entr=s.entr
                    ORDER BY g.gloss) AS sg
                ORDER BY entr,sens) AS gtxt
            FROM sens s WHERE s.entr=e.id ORDER BY s.sens) AS ss) AS gloss,
        (SELECT COUNT(*) FROM sens WHERE sens.entr=e.id) AS nsens,
        (SELECT p FROM is_p WHERE is_p.id=e.id) AS p,
        (NOT EXISTS (SELECT 1 FROM entr child WHERE child.dfrm=e.id)) AS leaf
    FROM entr e
    JOIN hdwds h on h.id=e.id);

-- Add new view "edroot".
CREATE OR REPLACE VIEW edroot AS (
    WITH RECURSIVE wt(id,dfrm) AS (
        SELECT id,dfrm FROM entr where dfrm is not null
        UNION
        SELECT e.id,e.dfrm
        FROM wt JOIN entr e ON wt.dfrm=e.id)
    SELECT * FROM wt);

COMMIT;
